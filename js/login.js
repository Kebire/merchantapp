/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

export default class Login extends Component {

  constructor(props){
    super(props);    
  }
  
  render() {
    return (
        <View>
      <Button
        title="Login as merchant"
        onPress={() => this.props.navigateToPage('merchant')}
      />
      <Button
      title="Login as consumer"
      onPress={() => this.props.navigateToPage('customer')}
    />
    </View>
    );
  }
}