/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import CustomerRow from './customer_row.js';
import CustomerLeftPanel from './LeftPanel.js';

export default class Login2 extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View>
          <CustomerLeftPanel></CustomerLeftPanel>
        </View>
        <View>
          <CustomerRow></CustomerRow>
          <CustomerRow></CustomerRow>
          <CustomerRow></CustomerRow>
          <CustomerRow></CustomerRow>
        </View>
      </View>
    );
  }
}