/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import MerchantRow from './merchant_row.js';
import LeftPanel from './LeftPanel.js';

export default class Login1 extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View>
          <LeftPanel></LeftPanel>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <MerchantRow></MerchantRow>
          <MerchantRow></MerchantRow>
          <MerchantRow></MerchantRow>
          <MerchantRow></MerchantRow>
          <MerchantRow></MerchantRow>
        </View>
      </View>
    );
  }
}