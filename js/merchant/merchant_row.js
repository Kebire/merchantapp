/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

export default class MerchantRow extends Component {

    constructor(props) {
        super(props);
    }

    renderColumn = (text) => {
        return(
            <Text style={{marginRight: 10}}>{text}</Text>
        )
    }

    render() {
        return (
            <View style={{flexDirection: 'row', borderRadius: 4,
            borderWidth: 4,
            borderColor: '#d6d7da'}}>
                {this.renderColumn('5th May 2017')}{this.renderColumn('Open')}{this.renderColumn('15,00 USD')}
            </View>
        );
    }
}