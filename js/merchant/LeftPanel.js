/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

export default class LeftPanel extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'gray' }}>
                <Button
                    title="TestMerchant"
                    onPress={() => this.props.navigateToPage('customer')}
                    color="gray"
                />
                <Button
                    title="TestMerchant"
                    onPress={() => this.props.navigateToPage('customer')}
                    color="gray"
                />
                <Button
                    title="TestMerchant"
                    onPress={() => this.props.navigateToPage('customer')}
                    color="gray"
                />
                <Button
                    title="TestMerchant"
                    onPress={() => this.props.navigateToPage('customer')}
                    color="gray"
                />
                <Button
                    title="TestMerchant"
                    onPress={() => this.props.navigateToPage('customer')}
                    color="gray"
                />
                <Button
                    title="TestMerchant"
                    onPress={() => this.props.navigateToPage('customer')}
                    color="gray"
                />
            </View>
        );
    }
}