/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Merchant from './js/merchant/merchant.js';
import Customer from './js/customer/customer.js';
import Login from './js/login.js';

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      active: 'login'
    }
  }

  navigateToPage = (page) => {
    this.setState({
      active: page
    });
  }

  render() {
    var active = this.state.active
    return (
      <View style={{flex: 1, flexDirection: 'row' }}>
        <View>
          {
            active === 'login' ? (
              <Login navigateToPage={this.navigateToPage} />
            ) : active === 'merchant' ? (
            <Merchant navigateToPage={this.navigateToPage} />
          ) : active === 'customer' ? (
            <Customer navigateToPage={this.navigateToPage} />
          ) : null}
        </View>

      </View>
    );
  }
}